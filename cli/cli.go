package cli

import (
	"github.com/jessevdk/go-flags"
	"os"
	"fmt"
	"bitbucket.org/Zero98/Go-Multiwallet/datastore"
)

const WALLET_VERSION = "0.0.1"

var(
	parser = flags.NewParser(nil, flags.Default)
	start Start
	version Version
	password Password
)

//All cli command
type (
	Version struct{}

    Start struct {
		Testnet bool `short:"t" long:"testnet" description:"use the test network"`
    }

    Password struct {
    	value string
	}
)

func init(){
	parser.AddCommand("start",
		"start the wallet",
		"The start command starts the wallet daemon",
		&start)

	parser.AddCommand("version",
		"print the version number",
		"Print the version number and exit",
		&version)

	parser.AddCommand("password",
		"set a user password",
		"Set a user password",
		&password)
}

func RunCli(){
	if _, err := parser.Parse(); err != nil {
		os.Exit(1)
	}
}

//************************************
//Implementation of the basic commands
//************************************
func (x *Version) Execute(args []string) error {
	fmt.Println(WALLET_VERSION)
	return nil
}

func (x *Start) Execute(args []string) error{
	m := wallet.GetCoinTypeDefaultMap()

}