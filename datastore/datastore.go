package wallet

type CoinType uint32

const (
	Bitcoin     CoinType = iota
	Ethereum
	BitcoinCash
)

func (c *CoinType) String() string {
	switch *c {
	case Bitcoin:
		return "Bitcoin"
	case BitcoinCash:
		return "Bitcoin Cash"
	case Ethereum:
		return "Ethereum"
	default:
		return ""
	}
}

func GetCoinTypeDefaultMap() map[CoinType]bool{
	return map[CoinType]bool{
		Bitcoin :     true,
		BitcoinCash : true,
		Ethereum:     true,
	}
}
