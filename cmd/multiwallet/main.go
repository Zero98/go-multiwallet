package main

import (
	"os"
	"os/signal"
	"fmt"
	"bitbucket.org/Zero98/Go-Multiwallet/cli"
)

var (
	//mw multiwallet.MultiWallet
)

func main(){
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	go func() {
		for range c {
			fmt.Println("Multiwallet shutting down...")
			os.Exit(1)
		}
	}()

	cli.RunCli()
}
